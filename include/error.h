#ifndef ERROR_H
#define ERROR_H

#include <string>

namespace error {
    void invalidInput();
    void invalidInput(const std::string input);
    void invalidFlag(const std::string flag);
    void nameRequired(const std::string flag);
    void cwdError(int err);
    void alreadyExists(const std::string fileName);
    void getMenuNameIndexFailed();
    void lastErrorMessage();
};

#endif // ERROR_H
