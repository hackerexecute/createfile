#ifndef FILECREATE_H
#define FILECREATE_H

#include <vector>
#include <string>

class FileCreate {
private:
    std::vector<std::string> _folders;
    std::string _filename;
    std::string _cwd;
    std::string _h, _cpp; // file extensions

    void setFolders();
    void createProjectFolders(const std::string parent);
    bool createMainSourceFile(const std::string parent);
    bool createDirectory(const std::string directory) const;
    void write(const std::string path, const std::string body) const;
    bool writeFile(const std::string path, const std::string body);

public:

    // CREATORS
    FileCreate();
    FileCreate(std::string fname);

    // MANIPULATORS
    void setCWD(const std::string cwd);

    // ACCESSORS
    std::string getCWD() const;
    
    bool createHeaderFile();
    bool createSourceFile(bool withHeader = false);
    void createClass();
    void createGitIgnore();
    void createReadMe();
    void createProject();
    void createMakefile();
};

#endif // FILECREATE_H
