#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>

namespace utilities {
    bool fileExists(const std::string path);
    bool moveFile(const std::string src, const std::string destination);
    
    namespace strings {
	    bool isEmpty(std::string name);
	    std::string toUpper(std::string);
	    std::string toLower(std::string);
	    std::wstring toWide(const std::string&);
    }
}

#endif // UTILITIES_H
