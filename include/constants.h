#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>
#include <map>

namespace constants {
    enum class SingleInput { HELP, Gitignore };
    enum class Flag { Header, Source, Class, Project, Makefile, };
    
    static std::map<SingleInput, std::string> singleInputs {
        { SingleInput::HELP, "HELP" },
        { SingleInput::Gitignore, "-g" }
    };
    static std::map<Flag, std::string> validFlags {
        { Flag::Header, "-h" },
        { Flag::Source, "-s" },
        { Flag::Class, "-c" },
        { Flag::Project, "-p" },
        { Flag::Makefile, "-m" }
    };
}

#endif  // CONSTANTS_H