#ifndef INPUTPROCESS_H
#define INPUTPROCESS_H

#include <string>

namespace inputprocess {
    bool isValidFlag(std::string flag);
    bool validateSingleInput(std::string input);
    void handleInput(std::string input);
    void handleInput(std::string flag, std::string name);
}

#endif // INPUTPROCESS_H
