#ifndef GENERATE_H
#define GENERATE_H

#include <string>

namespace generate {
    std::string headerBody(std::string fileName);
    std::string sourceBody(std::string fileName, bool withHeader);
    std::string mainBody();
    std::string makefileBody();
};

#endif // GENERATE_H