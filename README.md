# CreateFile
    A command line applicaton that processes arguments in order to generate files with template code
    such as: header and source files for a struct, class or namespace; a project directory with sub
    directories; and a template gitignore.
    
## Setup
- Download zip & extract.
- In command line navigate to extracted directory and run 'make'.
- Copy bin folder to any directory you want.
- Set PATH environment variable to directory containing bin: "C:\CreateFile\bin"
    
### Outstanding tasks
- [ ] Create Global class, Utility struct, and Options enum
- [ ] Debug unit tests
- [ ] Finish refactoring in refactor/namespaces branch
- [ ] Create shell script to install on linux environment
