
CC := g++
LIBC := ar rsv
INCLUDE := -I include
CPPFLAGS := -c -Wall

srcdir := src
objdir := build
bindir := bin
incdir := include
testdir := tests
static := $(objdir)\static

LIB := $(bindir)//static//libcf.a
EXE := $(bindir)//cf.exe
SRCS := $(wildcard $(srcdir)//*.cpp)
OBJS := $(patsubst $(srcdir)//%.cpp, $(static)//%.o, $(SRCS))

exe: $(EXE)

lib: $(LIB)

$(LIB): $(OBJS)
	$(LIBC) $@ $^
	@echo Compilation success

$(EXE): $(OBJS)
	$(CC) -g -o $@ $^ -static
	@echo Compilation success

$(static)//%.o: $(srcdir)//%.cpp
	$(CC) -g $(INCLUDE) $(CPPFLAGS) -o $@ $<


# Run Tests

tbuilddir := $(testdir)\build
TESTSRCS := $(wildcard $(testdir)//*.cpp)
TESTOBJS := $(patsubst $(testdir)//%.cpp, $(tbuilddir)//%.o, $(TESTSRCS))
TESTEXE := $(testdir)//runtests


tests: $(TESTOBJS)
	$(CC) $^ -L $(bindir)//static -l cf -o $(TESTEXE)

$(tbuilddir)//%.o: $(testdir)//%.cpp
	$(CC) -I ..//$(incdir) $(CPPFLAGS) -o $@ $<

moveExe:
	xcopy $(bindir)\cf.exe C:\Test /Y

.PHONY: clean
clean:
	@echo Cleaning object files
	del /f $(static)
	del /f $(tbuilddir)
