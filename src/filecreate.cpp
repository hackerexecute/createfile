#include <iostream>
#include <string>
#include <cerrno>
#include <fstream>
#include <direct.h>
#include <windows.h>
#include "../include/filecreate.h"
#include "../include/utilities.h"
#include "../include/generate.h"
#include "../include/error.h"
namespace str = utilities::strings;

FileCreate::FileCreate()
{
    setCWD(getCWD());
}

FileCreate::FileCreate(std::string fname) :_filename(fname)
{
    setCWD(getCWD());
    _h = ".h";
    _cpp = ".cpp";
}

void FileCreate::createGitIgnore()
{
    std::string path = "..\\.gitignore";
    
    if (utilities::fileExists(path)) {
        error::alreadyExists(".gitignore");
        exit(EXIT_FAILURE);
    }

    std::string dest = _cwd + "\\.gitignore";
    if (!utilities::moveFile(path, dest)) {
        error::lastErrorMessage();
    }
}

bool FileCreate::createHeaderFile()
{
    std::string directory = "\\include\\";
    std::string fileName = str::toLower(_filename) + _h;
    std::string headerBody = generate::headerBody(_filename);
    std::string path = _cwd + directory + fileName;
    createDirectory("include");
    
    if(!writeFile(path, headerBody)) {
        error::alreadyExists(fileName);
        return false;
    }
    return true;
}

void FileCreate::createClass()
{
    createHeaderFile();
    createSourceFile(true);
}

bool FileCreate::writeFile(const std::string path, const std::string body)
{
    if (!utilities::fileExists(path)) {
        write(path, body);
        return true;
    }
    return false;
}

bool FileCreate::createDirectory(const std::string dirName) const
{
    std::string path = _cwd + "\\" + dirName;
    std::wstring wPath = str::toWide(path);

    if (!CreateDirectoryW(wPath.c_str(), NULL))
        return true;

    return false;
}

void FileCreate::write(const std::string path, const std::string body) const
{
    std::ofstream file(path);
    if (file.good()) {
        file << body;
    }
    file.close();
}

bool FileCreate::createSourceFile(bool withHeader)
{
    std::string directory = "\\src\\";
    std::string fileName = str::toLower(_filename) + _cpp;
    std::string srcBody = generate::sourceBody(_filename, withHeader);
    std::string path = _cwd + directory + fileName;
    createDirectory("src");

    if (!writeFile(path, srcBody)) {
        error::alreadyExists(fileName);
        return false;
    }
    return true;
}

void FileCreate::createProject()
{
    if (createDirectory(_filename)) {
        error::alreadyExists(_filename);
        exit(EXIT_FAILURE);
    }
    setFolders();
    createProjectFolders("\\" + _filename);
    createMainSourceFile("\\" + _filename);
}

void FileCreate::createMakefile()
{
    std::string makefileBody = generate::makefileBody();
}

void FileCreate::setCWD(const std::string cwd)
{
    _cwd = cwd;
}

std::string FileCreate::getCWD() const
{
    char buffer[FILENAME_MAX];

    if (!_getcwd(buffer, sizeof(buffer))) {
        error::cwdError(errno);
        exit(EXIT_FAILURE);
    }
    return std::string(buffer);
}

void FileCreate::setFolders()
{
    _folders.push_back("bin");
    _folders.push_back("build");
    _folders.push_back("docs");
    _folders.push_back("include");
    _folders.push_back("lib");
    _folders.push_back("src");
    _folders.push_back("tests");
}

void FileCreate::createProjectFolders(const std::string parent)
{
    std::vector<std::string>::size_type index = 0;
    while (index != _folders.size()) {
        std::string dir = parent + "\\" + _folders[index++];
        createDirectory(dir);
    }
}

bool FileCreate::createMainSourceFile(const std::string parent)
{
    std::string directory = parent + "\\src\\";
    std::string fileName = "main" + _cpp;
    std::string mainBody = generate::mainBody();
    std::string path = _cwd + directory + fileName;
    
    if (!writeFile(path, mainBody)) {
        error::alreadyExists(fileName);
        return false;
    }
    return true;
}
