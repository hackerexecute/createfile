#include <string>
#include <fstream>
#include <algorithm>
#include <windows.h>
#include "../include/utilities.h"

namespace utilities {
    bool fileExists(const std::string path)
	{
    	std::ifstream ifile(path);
    	if (ifile.good()) {
			ifile.close();
			return true;
		}
		ifile.close();
		return false;
	}

	bool moveFile(const std::string src, const std::string destination)
	{
		std::wstring wsrc = strings::toWide(src);
		std::wstring wdest = strings::toWide(destination);

		if (!MoveFileW(wsrc.c_str(), wdest.c_str())) {
			return true;
		}
		return false;
	}

    namespace strings {
		bool isEmpty(std::string name)
		{
	    	return name.empty();
		}

		std::string toLower(std::string str)
		{
		    if (str.empty()) return str;
		    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		    return str;
		}
	
		std::string toUpper(std::string str)
		{
		    if (str.empty()) return str;
		    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
		    return str;
		}
	
		std::wstring toWide(const std::string& str)
		{
		    if (str.empty()) return std::wstring(L"");
		    int len;
		    int strLen = (int)str.length() + 1;
		    len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), strLen, 0, 0);
		    wchar_t* buffer = new wchar_t[len];
		    MultiByteToWideChar(CP_ACP, 0, str.c_str(), strLen, buffer, len);
		    std::wstring wstr(buffer);
	
		    delete[] buffer;
		    return wstr;
		}
    }
}
