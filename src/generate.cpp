#include <string>
#include <sstream>
#include "../include/generate.h"
#include "../include/utilities.h"
namespace str = utilities::strings;

namespace generate {
    extern const char tab[] = "\x20\x20\x20\x20";
    
    std::string headerBody(std::string fileName) {
        std::string alias = "INCLUDED_" + str::toUpper(fileName);
        std::stringstream ss;
        
	ss << "// " << str::toLower(fileName) << ".h\n";
        ss << "#ifndef " << alias << "\n";
        ss << "#define " << alias << "\n\n";

        fileName[0] = toupper(fileName[0]); // capitilize type name

        ss << "class " << fileName << " {\n";
        ss << "private:\n\n";
        ss << "public:\n";
        ss << tab << fileName << "();\n";
        ss << tab << "~" << fileName << "();\n";
        ss << "};\n\n";
        ss << "#endif";
        return ss.str();
    }
    
    std::string sourceBody(std::string fileName, bool withHeader) {
        std::stringstream ss;
        fileName[0] = toupper(fileName[0]);
        std::string headerName = str::toLower(fileName) + ".h";
        std::string includeHeader;

        if (withHeader) {
            includeHeader = "#include";
            includeHeader.append(" \"../include/" + headerName + "\"\n\n");
        }
        else {
            includeHeader = "";
        }
        
        ss << includeHeader;
        ss << fileName << "::" << fileName << "()\n{\n" << tab << "\n}\n\n";
        ss << fileName << "::~" << fileName << "()\n{\n" << tab << "\n}";
        return ss.str();
    }
    
    std::string mainBody() {
        std::stringstream ss;
        
        ss << "#include <iostream>\n\n";
        ss << "int main()\n{\n";
        ss << tab << "return 0;\n}";
        return ss.str();
    }
    
    std::string makefileBody() {
        return "SIKE";
    }
}
