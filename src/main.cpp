#include <iostream>
#include <string>
#include "../include/error.h"
#include "../include/inputprocess.h"
namespace iprocess = inputprocess;

void handleSingleInput(char* command);

int main(int argc, char **argv)
{
    if (argc < 2) {
		error::invalidInput();
		return 1;
    }

	if (argc == 2) {
		handleSingleInput(argv[1]);
		return 0;
	}
	else {
		std::string flag(argv[1]);
		std::string name(argv[2]);
		iprocess::handleInput(flag, name);
		return 0;
	}
	return 1;
}

void handleSingleInput(char* command)
{
	std::string cmd(command);
	iprocess::validateSingleInput(cmd);
	iprocess::handleInput(cmd);
}