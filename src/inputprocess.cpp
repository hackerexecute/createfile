#include <string>
#include <map>
#include "../include/inputprocess.h"
#include "../include/utilities.h"
#include "../include/constants.h"
#include "../include/filecreate.h"
#include "../include/error.h"
#include "../include/help.h"
namespace str = utilities::strings;
using namespace constants;

namespace inputprocess {
    bool isValidFlag(std::string flag)
    {
        flag = str::toLower(flag);
        std::map<SingleInput, std::string>::iterator it;
        for (it = singleInputs.begin(); it != singleInputs.end(); ++it) {
            if (it->second == flag) {
                return true;
            }
        }
        return false;
    }

    bool validateSingleInput(std::string input)
    {
        if (input.length() == 2) {
            if (str::toLower(input) != singleInputs[SingleInput::Gitignore]) {
                error::nameRequired(input);
                exit(EXIT_FAILURE);
            }
            return true;
        }
        else if (str::toUpper(input) == singleInputs[SingleInput::HELP]) {
            return true;
        }

        error::invalidInput(input);
        exit(EXIT_FAILURE);
    }

    void handleInput(std::string input)
    {
        if (str::toLower(input) == singleInputs[SingleInput::Gitignore]) {
            FileCreate fcreator;
            fcreator.createGitIgnore();
        }
        else {
            help::mainMenu();
        }
    }

    void handleInput(std::string flag, std::string name)
    {
        flag = str::toLower(flag);
        std::map<Flag, std::string>::iterator it;
        for (it = validFlags.begin(); it != validFlags.end(); ++it) {
            if (it->second == flag) break;
        }
        
        FileCreate fcreator(name);

        switch(it->first) {
            case Flag::Header:
                fcreator.createHeaderFile();
                break;
            case Flag::Source:
                fcreator.createSourceFile();
                break;
            case Flag::Class:
                fcreator.createClass();
                break;
            case Flag::Project:
                fcreator.createProject();
                break;
            case Flag::Makefile:
                fcreator.createMakefile();
                break;
            default:
                break;
        }
    }
}
