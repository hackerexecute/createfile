#include <iostream>
#include <string>
#include <cstring>
#include <windows.h>
#include "../include/error.h"
#include "../include/help.h"
#include "../include/utilities.h"

namespace error {

    void invalidInput()
    {
        std::cout << help::usage << std::endl;
    }

    void invalidInput(std::string input)
    {
        std::cerr << input << ": Not a valid input." << std::endl;
        std::cerr << help::usage << std::endl;
    }

    void nameRequired(const std::string flag)
    {
        std::cerr << flag << ": Name required for this flag." << std::endl;
        std::cerr << help::usage << std::endl;
    }

    void cwdError(int err)
    {
        std::string errmsg(strerror(err));
        std::cerr << "Error: " << errmsg << std::endl;
        std::cerr << "Unable to retrieve current working directory." << std::endl;
    }

    void alreadyExists(const std::string fileName)
    {
        std::cerr << fileName << ":\x20";
        std::cerr << "A directory or file with that name already exists." << std::endl;
    }
    
    void getMenuNameIndexFailed()
    {
        std::cerr << "Unexpected error occured in getMenuNameIndex()" << std::endl;
    }

    void lastErrorMessage()
    {
        DWORD errorId = ::GetLastError();
        if (errorId == 0) return;

        LPWSTR msgBuffer = nullptr;
        std::size_t size = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
                                          FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorId,
                                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                          (LPWSTR)&msgBuffer, 0, NULL);

        std::wstring message(msgBuffer, size);
        std::wcout << message << std::endl;
        LocalFree(msgBuffer);
    }
}
