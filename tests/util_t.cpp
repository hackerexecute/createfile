#include "catch.hpp"
#include <string>
#include "../include/util.h"

SCENARIO("handle and return correct value based on input", "[util]") {
    
    GIVEN("an empty string") {
        std::string emptyStr("");
        REQUIRE(util::isEmpty(emptyStr));
    }
    
    GIVEN("a non-empty string") {
        std::string notEmpty("Hello");
        REQUIRE_FALSE(util::isEmpty(notEmpty));
    }
}

SCENARIO("handle and return the correct transformed string", "[util][strings]") {
    
    GIVEN("an empty string") {
        std::string input = "", expected = "", actual;
        std::wstring wexpected = L"", wactual;
        
        WHEN("calling toLower()") {
            actual = util::toLower(input);
            REQUIRE(actual == expected);
        }
        
        WHEN("calling toUpper()") {
            actual = util::toUpper(input);
            REQUIRE(actual == expected);
        }
        
        WHEN("calling toWideString()") {
            wactual = util::toWideString(input);
            REQUIRE(wactual == wexpected);
        }
    }
    
    GIVEN("a non-empty string") {
        std::string input, expected, actual;
        std::wstring w_expected, w_actual;
        
        WHEN("calling toLower()") {
            input = "CONVERT ME";
            expected = "convert me";

            actual = util::toLower(input);
            REQUIRE(actual == expected);
        }
        
        WHEN("calling toUpper()") {
            input = "convert me";
            expected = "CONVERT ME";

            actual = util::toUpper(input);
            REQUIRE(actual == expected);
        }
        
        WHEN("calling toWideString()") {
            input = "convert me to wide string";
            w_expected = L"convert me to wide string";

            w_actual = util::toWideString(input);
            REQUIRE(w_actual == w_expected);
        }
    }
}