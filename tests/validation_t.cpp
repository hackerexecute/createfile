#include "catch.hpp"
#include "../include/create.h"

TEST_CASE("Input Arguments Validation", "[validation]") 
{
	std::string validFlags[3] = {"-c", "-p", "-m"};

	SECTION("flag argument does not exist") {
		std::string invalid = "-x";
		bool expected = false, actual;

		actual = create::isValidFlag(invalid);
		REQUIRE(expected == actual);
	}
	SECTION("flag argument is too short") {
		std::string tooshort = "-";
		bool expected = false, actual;

		actual = create::isValidFlag(tooshort);
		REQUIRE(expected == actual);
	}
	SECTION("flag argument is too long") {
		std::string toolong = "-asdfasdfasdf";
		bool expected = false, actual;

		actual = create::isValidFlag(toolong);
		REQUIRE(expected == actual);
	}
	SECTION("no flag argument is entered") {
		std::string noinput = "";
		bool expected = false, actual;

		actual = create::isValidFlag(noinput);
		REQUIRE(expected == false);
	}
	SECTION("flag argument is valid") {
		bool expected = true, actual;

		actual = create::isValidFlag(validFlags[0]);
		REQUIRE(expected == actual);
		actual = create::isValidFlag(validFlags[1]);
		REQUIRE(expected == actual);
		actual = create::isValidFlag(validFlags[2]);
		REQUIRE(expected == actual);
	}

	SECTION("no name agument entered but is required") {
		// Flag(s) that require a name
		std::string cflag = "-c", pflag = "-p", mflag = "-m";

		bool expected = true, actual;

		actual = create::isNameRequired(cflag);
		REQUIRE(expected == actual);
		actual = create::isNameRequired(pflag);
		REQUIRE(expected == actual);
		//actual = create::isNameRequired(mflag);
		//REQUIRE(expected == actual);
	}
}
