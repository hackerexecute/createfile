#include <string>
#include <fstream>
#include <windows.h>
#include "../include/util.h"
#include "../include/create.h"

namespace helper {
    
    extern const std::string cwd = "C:\\Projects\\CreateFileProject\\tests\\TestResults";
    
    bool fileCreated(std::string fileName, std::string ext) {
        std::string path = cwd + fileName + ext;
        std::ifstream ifile(path);
        return ifile.good();
    }

    bool folderCreated(std::string folderName) {
        std::string path = cwd + folderName;
        std::wstring wPath = util::toWideString(path);

        if (!CreateDirectoryW(wPath.c_str(), NULL))
            return true;

        return false;
    }
    
    void removeDirectory(std::string name) {
        std::string path = cwd + name;
        std::wstring wPath = util::toWideString(path);
        RemoveDirectoryW(wPath.c_str());
    }
}
