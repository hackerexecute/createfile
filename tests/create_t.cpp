#include "catch.hpp"
#include "helper.h"
#include "../include/create.h"

SCENARIO("Create Class", "[create][class]") {

    create::Create createObj;
    createObj.setFlag("-c");
    createObj.setFileName("MyName");
    createObj.setCWD(helper::cwd);
    createObj.create();

    GIVEN("create include and source directories") {
        bool expected = true, actual;
        
        actual = helper::folderCreated("\\include");
        REQUIRE(expected == actual);
        actual = helper::folderCreated("\\src");
        REQUIRE(expected == actual);
        
        AND_WHEN("include and source directories exist, they are not created") {
            bool expected = false, actual;
            
            actual = createObj.createDirectory("include");
            CHECK(expected == actual);
            actual = createObj.createDirectory("src");
            CHECK(expected == actual);
        }
    }
    
    GIVEN("create header and source file for name w/extensions") {
	bool expected = true, actual;

	actual = helper::fileCreated("\\include\\" + createObj.getFileName(), ".h");
	REQUIRE(expected == actual);
	actual = helper::fileCreated("\\src\\" + createObj.getFileName(), ".cpp");
	REQUIRE(expected == actual);

	AND_WHEN("header and source files already exist, don't overwrite") {   
	    bool expected = false;

	    // writeFile returns true if written, false if not
	    actual = createObj.writeFile("\\include\\", createObj.getFileName(), "");
	    REQUIRE(expected == actual);
	    actual = createObj.writeFile("\\src\\", createObj.getFileName(), "");
	    REQUIRE(expected == actual);
	}
    }
}

SCENARIO("Create Project", "[create][project") {
    
    GIVEN("input validation") {
	
    }
}
